---
numbersections: true
title: GitLab et lancement du projet
header-includes:
- |
  ```{=latex}
  \hypersetup{colorlinks,linkcolor=blue,urlcolor=blue}
  ```
---

Prise en main de GitLab
=======================

- Créez un compte sur [`gitlab.com`](https://gitlab.com/).
- Créez un projet personnel sur [`gitlab.com`](https://gitlab.com/).
- Peuplez le dépôt Git correspondant à ce nouveau projet :

  - Clonez le dépôt Git correspondant sur votre machine.
  - Faites un commit local (par exemple l'ajout d'une licence) sur la branche principale (probablement nommée `master`).
  - Poussez ce commit sur le serveur git (`git push`) sur la branche principale.
- Créez une *issue* sur votre projet (par exemple, "ajouter exemple de README").
- Créez une branche locale sur votre dépôt Git pour répondre à votre issue (par exemple, en créant dans cette branche un commit qui ajoute un README minimaliste).
- Poussez cette branche vers le serveur git.
- Créez une *merge request* pour fusionner votre branche dans la branche principale.
  Référencez votre issue précédente dans la merge request, de telle sorte que l'issue soit fermée quand la merge request est acceptée. Doc de référence :

  - [https://docs.gitlab.com/ee/user/project/issues/crosslinking_issues.html](https://docs.gitlab.com/ee/user/project/issues/crosslinking_issues.html)
- Acceptez la *merge request*. Vérifiez que votre modification a bien été prise en compte dans la branche principale du dépôt, et vérifiez que votre issue a bien été fermée.

\newpage
Création de votre dépôt Git de projet
=====================================

- Déterminez une personne dans votre groupe de projet chargé de créer le projet (au sens GitLab) sur [`gitlab.com`](https://gitlab.com/), et d'ajouter tous les membres du projet dedans (ainsi que moi : [`mpoquet`](https://gitlab.com/mpoquet)).
- Écrivez dans un fichier README une description **concise** du projet que vous souhaitez réaliser :

  - Que comptez-vous développer ?
  - Partez-vous d'un code existant ou de zéro ?
  - Quelles sont les différentes fonctionnalités envisagées ?
    Lesquelles considérez-vous comme obligatoires ?
  - Quelles étapes envisagez-vous pour le développement du projet ?
  - Quelles méthodes ou techniques envisagez-vous pour vous assurer d'une certaine qualité logicielle ?

- (Bonus) Mettez en place le lancement automatique du script suivant lors d'un `push` sur votre brance principale : `python --version`. Doc de référence :

  - Gitlab CI : [https://docs.gitlab.com/ee/ci/](https://docs.gitlab.com/ee/ci/)
  - Syntaxe `.gitlab-ci.yml` : [https://docs.gitlab.com/ee/ci/yaml/](https://docs.gitlab.com/ee/ci/yaml/)
