---
numbersections: true
title: Projet de qualité logicielle
header-includes:
- |
  ```{=latex}
  \hypersetup{colorlinks,linkcolor=blue,urlcolor=blue}
  \usepackage{arydshln}
  ```
---

Description du projet
=====================

Le but de ce projet est d'appliquer les bonnes pratiques de qualité vues dans les cours et TP précédents.
Le sujet de votre projet est libre sous les contraintes évoquées précédemment.

Le logiciel que vous allez créer ou modifier n'est pas le cœur de ce projet.
Comme le montre la grille de notation \ref{grille_notation},
l'évaluation de votre projet dépend surtout de la démarche de qualité adoptée tout au long de votre projet et non de l'état final de votre rendu.

Voici une liste de libertés/contraintes pour le développement de ce projet.

- Vous devez utiliser un workflow Git.
  Ce workflow doit comprendre des branches de fonctionnalités dont l'intégration doit rester visible dans l'historique Git du projet.
  Un exemple de tel workflow est [Gitflow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow), mais vous êtes libres d'en utiliser un autre qui respecte les mêmes contraintes.
- Faites en sorte que votre historique Git soit lisible.
  En particulier, vos messages de commits doivent être clairs et concis.
  Privilégiez l'intégration de branches *fast forward* tout en conservant un commit de fusion, afin de maximiser la lisibilité de votre historique et la traçabilité de vos intégrations.
- Vous ne devez **pas** écrire tout votre code d'un coup puis le tester.
  Vous devez à la place vous assurer de maintenir une qualité tout en faisant évoluer le code — c'est toute la difficulté d'un développement réel.
- Votre projet doit avoir une documentation minimum (voir table \ref{grille_notation}).
- Vous devez faire en sorte que vos bugs et corrections de bugs soient traçables.
  Le plus simple pour ça est est de vous servir des [issues](https://docs.gitlab.com/ee/user/project/issues/index.html) GitLab.
- Vous pouvez profiter de ce projet pour vous essayer au [Test-Driven Development](https://fr.wikipedia.org/wiki/Test_driven_development) ou au [Pair programming](https://fr.wikipedia.org/wiki/Programmation_en_bin%C3%B4me).
- Vous pouvez vous servir des outils de GitLab pour suivre l'avancement de votre projet, notamment des [milestones](https://docs.gitlab.com/ee/user/project/milestones/) et du [kanban](https://docs.gitlab.com/ee/user/project/issue_board.html).


Étapes du projet
================

Planification
-------------
Cette phase du projet a déjà été entamée dans un TP précédent.
Les principales objectifs sont de déterminer :

- Quel logiciel développer ?
- Quelles sont les différentes fonctionnalités du projet ?
- Quelles fonctionnalités doivent être présentes, lesquelles sont optionnelles ?
- Dans quel ordre faire ces fonctionnalités ?
- À quelles étapes du développement comptez-vous faire une *release* ? \
  Comment prévoyez-vous de numéroter ces *releases* ?

Savoir précisement ce qu'on souhaite développer et avoir une vue d'ensemble des phases de développement qui nous attendent est essentiel pour que le développement soit efficace.

**Il vous est demandé ici de rédiger votre planification initiale du projet.**
Votre *rapport* de planification doit être mis sur votre dépôt Git (dans un format lisible sans outil particulier comme du Markdown) et accessible simplement dans votre projet GitLab — par exemple via un lien depuis le README principal de votre projet.

Premiers développements, tests de régression
--------------------------------------------
Cette phase a pour but de mettre en place de bonnes bases pour la suite du développement.
Commencez par choisir une licence pour votre projet — [choosealicence](https://choosealicense.com/) peut faciliter votre prise de décision si vous souhaitez faire du libre.
Le but est de mettre en place le plus tôt possible des tests de régression sur votre code, et d'automatiser le lancement de ces tests.
La raison de faire ça le plus tôt possible est que vous puissiez vous reposer sur ces tests pour développer plus sereinement toute la suite du projet : si vous cassez une ancienne fonctionnalité en en développement une nouvelle, vous devriez pouvoir le détecter si votre base de test est suffisante — mieux vaut détecter et corriger une erreur très tôt que trop tard.

Dès que vous pensez avoir une base de code testée, vous pouvez faire une *release* initiale de votre projet. Référez-vous à [Semantic Versioning](https://semver.org/lang/fr/) pour numéroter votre version initiale selon l'état d'avancement de votre projet à ce moment-là.
Il est aussi utile de créer un changelog à ce moment-là (voir [keep a changelog](https://keepachangelog.com/en/1.0.0/)). Votre changelog sera probablement vide à cette étape, mais le créer très tôt permet de le mettre à jour plus facilement ensuite.

Cycle de développement de fonctionnalités
-----------------------------------------
La suite du projet est une suite de développement de fonctionnalités.

Chaque fonctionnalité doit être développée dans une branche dédiée.
Avant d'intégrer la fonctionnalité dans votre branche de développement principale,
vous devez vous assurer qu'elle ne casse aucun des tests existants, et vous assurer
que la nouvelle fonctionnalité est testée raisonnablement.

Après avoir intégré un certain nombre de features,
selon votre planificiation initiale ou lorsque vous pensez que c'est pertinent,
effectuez une nouvelle *release* de votre projet — référez-vous encore à [Semantic Versioning](https://semver.org/lang/fr/) pour déterminer comment numéroter votre version.
Vous êtes invités à *packager* et à *containeriser* votre nouvelle version.

Rendu final
-----------
Si vous avez suivi une démarche de qualité tout au long du développement,
vous n'avez rien de particulier faire en plus à cette étape-là.
Assurez-vous juste l'ensemble de votre documentation est à jour et faites une nouvelle *release*.

Critères d'évaluation
=====================
La grille de notation est donnée en table \ref{grille_notation}.
Cette section détaille certains points de cette table.

**Travail continu** et **Répartition du travail**.
Effectuer un travail tout au long de la durée vie du projet sera récompensé,
alors que faire tout le code la nuit avant la soutenance ne le sera pas.
Un travail réparti entre les différents membres du projets sera récompensé,
contrairement à ce qu'un seul membre fasse la totalité du travail seul.

**Messages** et **tailles** des commits.
Le message d'un commit doit résumer ce que fait le commit de manière claire et concise.
En lisant l'historique Git, on doit pouvoir avoir une idée de ce que fait un commit sans être obligé de lire son contenu.
Concernant la taille d'un commit, évitez de faire des commits gigantesques qui pourraient être découpés en sous-parties elles-mêmes intéressantes. En particulier, s'il est possible dans la suite du projet que l'on souhaite annuler une sous-partie d'un commit, il est sans doute préférable de découper ce commit en sous-commits.

**Pertinence des tests faits**.
Les différentes parties du code de votre logiciel n'ont pas les mêmes besoins de tests.
Typiquement, des tests unitaires sur une fonction complexe au coeur du projet sont très intéressants, tandis que des tests unitaires sur des *getters and setters* l'est moins.
De même, selon la nature de votre projet, le type de test à privilégier ne sera pas le même.
Si vous développez une bibliothèque, tester son API externe est primordial, alors que c'est moins le cas d'un programme exécutable — même si ça reste important.
Si vous développez un outil réseau, je m'attends à ce qu'une partie de vos tests fasse des communications réseau avec votre outil.
Au final, ce critère de notation correspond à la question suivante : est-ce que les parties critiques de votre logiciel sont solidement testées ?

**Environnement de test contrôlé**.
Il est important de savoir avec quelles versions de vos différentes dépendances votre projet est censé marcher. Ce critère récompense la présence d'environnement logiciels bien définis et contrôlés pour exécuter vos tests (par exemple via [virtualenv](https://virtualenv.pypa.io/en/latest/), [Docker](https://fr.wikipedia.org/wiki/Docker_(logiciel)) ou [Nix](https://en.wikipedia.org/wiki/Nix_package_manager)).

**Packaging**.
Cette partie de la notation évalue si votre projet utilise les outils standard de votre langage pour être distribué. Par exemple en Python, je m'attends à ce que votre projet ait un `setup.py` et vous pouvez rendre votre package disponible sur [PyPI](https://pypi.org/) si vous le souhaitez (ce n'est pas obligatoire). Bien définir vos dépendances (les lister et lister leurs versions) est important pour les utilisateurs de votre logiciel. De plus, fournir une version containerisée (par exemple via Docker) de votre logiciel est récompensé.

**Documentation**.
Il faut que *ce qu'est* votre projet soit évident à un utilisateur qui arrive sur votre dépôt GitLab. De plus, il faut que des instructions basiques pour installer votre logiciel et s'en servir soient facilement accessibles. Pour un programme exécutable, je m'attends à ce qu'une option `--help` indique très brièvement ce qu'est votre programme, ainsi les options disponibles de son interface en ligne de commande. \
Il est ensuite primordial de tenir un **changelog** pour vos utilisateurs, ainsi qu'une documentation du code lui-même pour les développeurs et potentiels futurs contributeurs de votre code.

**Gestion de projet** dans la grille de notation correspond à la question : est-ce que votre manière de gérer le projet favorise la qualité du logiciel produit ?
Une **planification** existante et efficace sera récompensée, contrairement à un manque de planification ou à une planification beaucoup trop détaillée pour l'envergure du projet.
Le choix de privilégier l'amélioration de la qualité du code existant, plutôt que d'ajouter de nouvelles features, sera récompensé. Il en va de même pour le choix d'intégrer du code uniquement après qu'il ait été bien testé.
La **traçabilité** des bugs est importante pour vos utilisateurs : ils risquent d'être plus tard confrontés à un bug que vous avez corrigé, et il est important pour eux de savoir à partir de quelle version (commit) de votre logiciel le problème est résolu — les issues de GitLab sont pour cela particulièrement utiles.
La traçabilité de l'avancement du projet est plutôt pour là pour vous aider à choisir efficacement sur quelle fonctionnalité se pencher à chaque étape du projet. Rendre cet avancement accessible aux utilisateurs est pratique si l'on souhaite qu'ils puissent contribuer au projet en ayant une idée de la *vision* du projet.

**Soutenance** correspond à la présentation interactive que vous ferez de votre projet, en dernière séance. Vous devrez concisément présenter votre projet et une démonstration (en direct ou vidéo) de son fonctionnement, puis montrer quelle méthodes vous avez utilisé pour assurer une qualité lors du développement du projet. La franchise et l'honnêteté sur le rôle de chacun dans le projet seront récompensées.

```{=latex}
\label{sec_eval}
```

```{=latex}
\begin{table}[]
\caption{Grille de notation}
\label{grille_notation}
\begin{tabular}{|c|r|r|}
\hline
Général           &                                       & \textbf{4}  \\
\hdashline[1pt/5pt]
                  & Difficulté du sujet                   & 2  \\
                  & Travail continu                       & 1  \\
                  & Répartition du travail                & 1  \\
\hline
Git               &                                       & \textbf{7}  \\
\hdashline[1pt/5pt]
                  & Messages de commits                   & 2  \\
                  & Taille des commits                    & 1  \\
                  & Branches de features tracées          & 2  \\
                  & Lisibilité de l’historique            & 2  \\
\hline
Test              &                                       & \textbf{15} \\
\hdashline[1pt/5pt]
                  & Tests automatiques                    & 4  \\
                  & Lancement des tests lors d’un push    & 2  \\
                  & Pertinence des tests faits            & 4  \\
                  & Environnement de test contrôlé        & 2  \\
                  & Couverture                            & 2  \\
                  & Lançable en une commande ?            & 1  \\
\hline
Packaging         &                                       & \textbf{5}  \\
\hdashline[1pt/5pt]
                  & Package standard du langage cible     & 2  \\
                  & Dépendances bien définies             & 2  \\
                  & Application containerisée             & 1  \\
\hline
Documentation     &                                       & \textbf{7}  \\
\hdashline[1pt/5pt]
                  & Que fait le projet ?                  & 2  \\
                  & Comment l’installer/déployer ?        & 1  \\
                  & Comment l’utiliser ?                  & 1  \\
                  & Changelog                             & 2  \\
                  & Documentation du code                 & 1  \\
\hline
Génie logiciel    &                                       & \textbf{4}  \\
\hdashline[1pt/5pt]
                  & Redondance de code évitée             & 1  \\
                  & KISS ?                                & 1  \\
                  & Sépération des préoccupations         & 1  \\
                  & Code lisible ?                        & 1  \\
\hline
Gestion de projet &                                       & \textbf{13} \\
\hdashline[1pt/5pt]
                  & Planification                         & 2  \\
                  & Privilégier bugfix à nouvelle feature & 4  \\
                  & Contrôle qualité en intégrant feature & 4  \\
                  & Traçabilité des bugs                  & 2  \\
                  & Traçabilité de l’avancement du projet & 1  \\
\hline
Soutenance        &                                       & \textbf{7}  \\
\hdashline[1pt/5pt]
                  & Présentation du sujet                 & 1  \\
                  & Démonstration / vidéo d’utilisation   & 1  \\
                  & Présentation de la démarche qualité   & 4  \\
                  & Franchise / honnêteté                   & 1  \\
\hline
\end{tabular}
\end{table}
```

