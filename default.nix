{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/20.03.tar.gz") {}
}:

let
  jobs = rec {
    inherit pkgs;

    slides = pkgs.stdenv.mkDerivation {
      pname = "git-slides";
      version = "0.1.0";
      nativeBuildInputs = with pkgs; [
        texlive.combined.scheme-full
        pandoc
        ninja
        inkscape
        asymptote
      ] ++ r-shell.buildInputs;
      src = pkgs.lib.sourceByRegex ./. [
        "^build\.ninja$"
        "^fig$"
        "^fig/.*\.asy$"
        "^fig/.*\.R$"
        "^fig/.*\.csv$"
        "^img$"
        "^img/.*\.eps"
        "^img/.*\.svg"
        "^img/.*\.png"
        "^slides$"
        "^slides/version-control\.md$"
        "^slides/.*\.sty$"
      ];
      buildPhase = ''
        ninja
      '';
      installPhase = ''
        mkdir -p $out
        mv slides/version-control.pdf $out/
      '';
    };

    autorebuild-shell = pkgs.mkShell rec {
      name = "autobuild-shell";
      buildInputs = [
        pkgs.entr
      ] ++ slides.nativeBuildInputs;
      shellHook = ''
        ls build.ninja slides/*.md exercises/*.md slides/*.sty fig/*.R fig/*.asy | entr -s "ninja ; killall -HUP --regexp '(.*bin/)?llpp'"
      '';
    };

    r-shell = pkgs.mkShell rec {
      name = "r-shell";
      buildInputs = [
        pkgs.R
        pkgs.rPackages.docopt
        pkgs.rPackages.tidyverse
        pkgs.rPackages.timelineS
        pkgs.rPackages.viridis
      ];
    };

    python-shell = pkgs.mkShell rec {
      buildInputs = [
        pkgs.python3
        pkgs.python3Packages.ipython
        pkgs.python3Packages.pytest
        pkgs.python3Packages.pytestcov
        pkgs.python3Packages.pytest-html
      ];
    };
  };
in
  jobs
