---
title: Qualité logicielle
author: Millian Poquet
theme: Estonia
date: 2021-12-09
lang: fr
header-includes:
- |
  ```{=latex}
  \usepackage{subcaption}
  \setbeamertemplate{footline}[frame number]
  \renewcommand\dots{\ifmmode\ldots\else\makebox[1em][c]{.\hfil.\hfil.}\thinspace\fi}
  \hypersetup{colorlinks,linkcolor=,urlcolor=estonian-blue}
  \graphicspath{{../fig/},{../img/}}

  % Fix pandoc shenanigans
  \setbeamertemplate{section page}{}
  \setbeamertemplate{subsection page}{}
  \AtBeginSection{}
  \AtBeginSubsection{}
  ```
---

# Informations pratiques

### Intervenants
Millian Poquet

- `millian.poquet@univ-grenoble-alpes.fr` \
  `millian.poquet@inria.fr`
- Postdoc/Ingénieur de recherche à l'UGA/Inria
- Développement/maintenance logicielle (toute la chaîne)
- Domaine : systèmes distribués, simulation, gestion de ressources
- Études : DUT, licence, master, doctorat
- Enseignant depuis 2010 (algo, programmation, big data...)

\vspace{10mm}
David Beniamine

- Vous devez déjà le connaître :)

### Historique de ce cours
2019-2020 : DevOps

- Difficile sans concepts/expérience de qualité logicielle
- $\rightarrow$ Besoin de plus d'heures et d'un cours à part

```{=latex}
\begin{center}
  \includegraphics[width=.9\textwidth]{devops-toolchain.pdf}
\end{center}
```

### Objectifs
- Concepts
- Technologies
- Autonomie
- Critique

### Organisation
Planning

- 2021-12-09 (6h) : Gestion de version
- 2022-01-03 (6h) : Test et intégration continue
- 2022-01-20 (6h) : Projet
- 2022-02-03 (6h) : Projet

\vspace{7mm}
Comment se passent les séances ?

- Centré sur la pratique (TP/projet)
- En présentiel tant que possible
- (À distance, salle centrale sur BBB)
- (Cours vidéo (ou en direct sur BBB))

### Évaluation
- Projet
  - Développement nouveau logiciel
  - En groupe (2 ou 3 personnes)
  - Rendus intermédiaires de code
  - Soutenance
- (Rendu de certains TP)
- (Rendu d'exercices de cours)


# Qualité logicielle
### Définition ?

Dur à définir ! Problème multiobjectif...

Tentatives de normalisation — *e.g.*, ISO/CEI 9126

- Capacité fonctionnelle : répond aux besoins fonctionnels ?
- Fiabilité : pannes ?
- Utilisabilité : requiert peu d’effort à l’utilisation ?
- Efficacité : performances ?
- Maintenabilité : requiert peu d’effort à son évolution ?
- Portabilité : transférable dans un autre environnement ?

### Approche de ce cours

Pragmatique, pas théorique

- Quels outils sont essentiels pour une bonne qualité ?
- Quelles méthodes de travail améliorent la qualité ?

\vspace{7mm}
Aspects traités

- Contrôle de version
- Contrôle d'environnement
- Méthodes de travail
- Test
- Automatisation

# Gestion de version
###
Voir sous-cours dédié.

# Forge logicielle
### Définition
Logiciel qui regroupe outils et services pour le développement.

\vspace{0.5cm}
Outils courants.

- Serveur de gestion de version
- Bug tracker
- Espace de discussion (vue à long terme, feature requests...)
- Pages web / wiki
- Exécution automatique de tests
- Suivi de métriques du projet

### GitLab
La forge que l'on va utiliser.

- Basé sur Git
- Interface web (clicodrome ou REST)
- Concurrent à GitHub depuis 2014
- Version communautaire libre (MIT)
- Architecture distribuée modulaire

\vspace{1cm}
**Non centralisé.** **Beaucoup d'instances (la vôtre ?)**

### GitLab CI
Système d'exécution de code quand le dépôt est modifié.

- Déclencement configurable. Par exemple :
  - Sur chaque commit de la branche de développement
  - Chaque nuit sur la branche de développement
  - Chaque dimanche sur la branche de la dernière *release* stable
  - Quand on souhaite intégrer une nouvelle *feature*

- Environnement configurable dans chaque situation
  - Via l'utilisation de conteneurs (Docker)
  - Indirectement via la sélection de machines pré configurées

- On peut connecter ses propres *runners* au serveur GitLab

# Tests
### Peut-on prouver qu'un logicielle *marche* ?
Dans l'idéal, on voudrait garantir qu'un logiciel marche[^marche].

Malheureusement, c'est impossible automatiquement \
(cf. [Théorème de Rice](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Rice)).

\pause
On peut par contre étudier un logiciel donné en **prouvant** certaines propriétés dessus (cf. [Méthodes formelles](https://fr.wikipedia.org/wiki/M%C3%A9thode_formelle_(informatique))). Ces méthodes sont robustes, elles ont été utilisées sur certains systèmes critiques (*e.g.*, contrôle de la ligne 14 du métro parisien ou de processeurs).

\pause
On peut aussi modéliser un système puis **vérifier** que certaines propriétés sont garanties dans ce modèle (cf. [Model checking](https://en.wikipedia.org/wiki/Model_checking)). En pratique, on explore un graphe (gigantesque) de scénarios de l'[automate fini](https://fr.wikipedia.org/wiki/Automate_fini) du système en vérifiant que des formules écrites en [logique temporelle](https://fr.wikipedia.org/wiki/Logique_temporelle) restent vraies.

[^marche]: Définir ce qu'on entend par *marcher* se spécifie souvent projet par projet.

### Que sont les tests dans tout ça ?
Une approche **simple** pour de **faibles** garanties.

```{=latex}
\vspace{5mm}
\begin{block}{Principe}
\begin{itemize}
  \item Exécuter le code réel sur des scénarios bien définis.
  \item Vérifier que le comportement observé est celui attendu.
\end{itemize}
\end{block}

\vspace{5mm}

\begin{exampleblock}{Exemples}
\begin{itemize}
  \item Les valeurs retournées d'une fonction sont les bonnes ?
  \item Le programme a \textbf{bien} terminé ? (code de retour normal)
  \item Le programme a terminé en moins de 100 millisecondes ?
  \item L'état du service est celui attendu après cette requête ?
  \item L'erreur renvoyée est celle attendue ?
\end{itemize}
\end{exampleblock}
```

### Critique des tests

#### Limites

*Testing shows the presence, not the absence of bugs.*\
(Edsger Dijkstra)

#### Avantages

- Scénarios d'utilisation = exemples pour utilisateurs.
- Détecter une cassure (*break*) des scénarios testés devient trivial.
- Évaluation des performances du programme.

### Classification des tests (1)

De nombreuses classifications... \
Voyons les types de test très importants.
\vspace{3mm}

#### Test unitaire
Test d'une (petite) portion d'un logiciel **en isolation**.

\vspace{3mm}
Exemple : entrées/sorties/erreurs d'une fonction/module.

#### Test de (non) régression
Re-exécuter des tests lors d'une modification pour éviter de *break*.

\vspace{3mm}
**Au cœur** des méthodologies *récentes* de suivi de qualité.

### Classification des tests (selon l'angle d'approche)

#### Isolation des composants ?
Test unitaire, d'intégration, *système*...

#### Connaissance du code testé ?

Tests dits en boîte blanche/grise/noire.

#### *Fonctionnel* ou non ?
Non fonctionnel s'intéresse au logiciel dans son environnement.

Exemples : test de charge, de performance, de sécurité...

#### Comment décider qu'un test passe ou non ?

- Quels résultats accepter quand plusieurs sont équivalents ?
- Faut-il que l'algo produise la solution optimale à un problème ou est-ce qu'une solution de qualité *raisonnable* suffit ?

### Que faut-il tester ?
Impossible de répondre de manière générique. Nombreux facteurs.

- Criticité du logiciel.
- Méthodologie de travail utilisée par les développeurs.
- Intuitions et rigueur des développeurs.

Quelques exemples de bonnes pratiques.

- *Bien* découper en composants, et *bien* les tester.
- Tout algo qui semble non trivial.
- Si système à états inévitable, tester les états du système.
- Ne pas oublier l'interface externe du logiciel.

  - API pour le cas d'une bibliothèque ou d'un service.
  - CLI pour un programme.
  - Si utilisateur=humain, l'ergonomie se teste aussi.

### Testception

Les tests étant eux-mêmes du code, les tester aurait du sens...
Faut-il le faire ? Comment ?

Souvent, on se sert juste de certaines métriques pour vérifier qu'un test utilise bien la bonne sous-partie du logiciel qui nous intéresse.

La plus courante est la couverture (*coverage*), qui permet de savoir quelles instructions/fonctions/branches[^branch-coverage] sont utilisées par les différents tests (et combien de fois elles sont appelées).

Au niveau de tout un projet, le *coverage* permet de voir quelles parties du code sont faiblement testées. Suivre son évolution aide à détecter de nouveaux codes morts ou peu testés.

[^branch-coverage]: Plutôt que de compter les instructions appelées, on peut les compter au sein de l'arbre syntaxique de la fonction — *e.g.*, savoir si un code qui suit un branchement `if`/`else` n'est appelé qu'après la partie `if` peut montrer un problème (ou non !)


# Méthodes de travail
### Human in the loop

#### Citation
*Programs should be written for people to read, \
and only incidentally for machines to execute.*

\vspace{2mm}
(Harold Abselson, Gerald Jay Sussman)

#### Citation
*Any fool can write code that a computer can understand. \
Good programmers write code that humans can understand.*

\vspace{2mm}
(Martin Fowler)

#### Citation
*If the computer doesn't run it, it's broken. \
If people can't read it, it will be broken. Soon.*

\vspace{2mm}
(Charlie Martin)

### Ne pas se répéter

```{=latex}
\begin{block}{Principe}
Éviter toute redondance de code.
\end{block}

\vspace{5mm}

\begin{exampleblock}{Pourquoi ?}
Tout code, qu'il comporte actuellement un bug ou non, peut causer des soucis plus tard.

\vspace{3mm}
Si un bout de code dupliqué est corrigé, il est probable qu'on oublie d'appliquer le correctif sur ses réplicats.
\end{exampleblock}
```

### Keep It Simple, Stupid (KISS)

```{=latex}
\begin{block}{Principe}
Toute complexité \textbf{non indispensable} devrait être évitée.
\end{block}

\begin{exampleblock}{Pourquoi ?}
Comprendre (et donc maintenir) un code simple est plus facile.
\end{exampleblock}

\begin{block}{Conséquences}
\begin{itemize}
  \item Impacte le choix d'algorithmes.
  \item Impacte le choix de structures de données.
  \item Impacte le choix de paradigmes et de modularisation du code.
  \item Limite le \textit{feature creep}.
\end{itemize}
\end{block}
```

### Test Driven Development (TDD)
```{=latex}
\begin{block}{Principe}
\begin{itemize}
\item Concevoir un logiciel pas à pas, par cycles itératifs.
\item Écrire un test avant d'implémenter quelque chose.
\item Alterner entre écriture des tests et du code en continu.
\end{itemize}
\end{block}

\only<1>{
\begin{center}
  \includegraphics[width=.6\textwidth]{cycle-tdd.pdf}
\end{center}}
\only<2>{\vspace{14.8mm}
\begin{exampleblock}{Intérêts}
\begin{itemize}
  \item Permet de vérifier un besoin avant de coder.
  \item Permet de vérifier que l'API est satisfaisante avant de coder.
  \item Documente comment appeler chaque code.
  \item Fournit des tests de régression pour chaque code.
\end{itemize}
\end{exampleblock}}
```

### Pair Programming

```{=latex}
\begin{block}{Principe}
Travailler en binôme sur une seule machine (ou partage d'écran).
\begin{itemize}
  \item A développe l'application.
  \item B observe et critique chaque décision, détecte soucis.
\end{itemize}

Échanger régulièrement les rôles.
\end{block}

\begin{exampleblock}{Intérêts}
\begin{itemize}
  \item Détection de soucis très efficace.
  \item Critique instantanée. Réduit cycles pour manque de qualité.
  \item Force communication/raisonnement/justification des choix.
\end{itemize}
\end{exampleblock}
```

# Contrôle d'env
### Contrôle d'environnement logiciel

```{=latex}
\begin{block}{Principe}
Définir et isoler les environnements logiciels utilisés.
\begin{itemize}
  \item Pour développer l'application.
  \item Pour tester l'application.
  \item Pour déployer l'application.
\end{itemize}
\end{block}

\begin{exampleblock}{Intérêts}
\begin{itemize}
  \item Coût d'entrée pour un nouveau développeur réduit.
  \item Documente les dépendances et leurs versions.
  \item Tester différentes combinaisons devient simple.
\end{itemize}
\end{exampleblock}
```
